<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\questionnaire;
use App\question;
use App\response;
use App\Users;

class QuestionnaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function __construct()
     {
         $this->middleware('auth');
     }

    public function index()
    {
      $questionnaires = questionnaire::all();

      return view('admin/questionnaire', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      return view('admin/questionnaire/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $this->validate($request, [
        'title' => 'bail|required|unique:questionnaires|min:3|max:255',
        'description' => 'bail|required|min:3|max:255',
        'no_questions' => 'bail|required|min:1|max:20',
      ]);
      $input = $request->all();

      questionnaire::create($input);

      return redirect('admin/question/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $questionnaire = questionnaire::where('id', $id)->first();
      $questions = questionnaire::lists('no_questions', 'id');

      if(!$questionnaire)
      {
        return redirect('admin/questionnaire');
      }
      return view('admin/questionnaire/show', compact('questions'))->withquestionnaire($questionnaire);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $questionnaire = questionnaire::findOrFail($id);

      return view('admin/questionnaire/edit', compact('questionnaire'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $questionnaire = questionnaire::findOrFail($id);

      $questionnaire->update($request->all());

      return redirect('admin/questionnaire');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = questionnaire::find($id);

        $questionnaire->delete();

        return redirect('admin/questionnaire');
    }
}
