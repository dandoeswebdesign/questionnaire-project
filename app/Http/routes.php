<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/






Route::get('/auth', function () {
    return view('auth');
});
Route::get('/login', function () {
    return view('login');
});

Route::get('/register', function () {
    return view('register');
});


Route::auth();

Route::get('/home', 'HomeController@index');

Route::group(['middle' => ['web']], function () {

  Route::resource('admin/questionnaire', 'QuestionnaireController');
  Route::resource('admin/question', 'QuestionController');
});
Route::resource('/', 'HomeController');
