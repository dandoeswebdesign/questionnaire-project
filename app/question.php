<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
  protected $fillable = [
        'question',
        'question_type',
        'questionnaire_id',
    ];
  public function questions()
  {
    return $this->belongsTo('App\questionnaire');
    return $this->hasMany('App\response');
  }
  public function questionnaires()
  {
    return $this->hasMany('App\quesiton');
  }
}
