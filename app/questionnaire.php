<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class questionnaire extends Model
{
  protected $fillable = [
        'title',
        'description',
        'no_questions',
        'user_id',
    ];
  public function questionnaires()
  {
    return $this->belongsTo('App\User');
    return $this->hasMany('App\question');
  }
}
