<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class response extends Model
{
  protected $fillable = [
        'response',
        'question_id'
    ];
  public function responses()
  {
    return $this->belongsTo('App\questionnaire');
    return $this->hasMany('App\response');
  }
}
