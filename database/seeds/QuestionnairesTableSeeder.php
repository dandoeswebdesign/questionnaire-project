<?php

use Illuminate\Database\Seeder;

class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('questionnaires')->insert([
            ['id' => 1, 'title' => "Test 1", 'description' => "Testing a seed"],
        ]);
    }
}
