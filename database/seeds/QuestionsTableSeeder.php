<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // disable foreign key constraints
      DB::table('institutes')->truncate();
      DB::statement('SET FOREIGN_KEY_CHECKS = 1'); // enable foreign key constraints
      DB::table('questions')->insert([
            ['id' => 1, 'question' => "Question 1", 'question_type' => "Text", 'questionnaire_id' => "1"],
            ['id' => 2, 'question' => "Question 2", 'question_type' => "Text", 'questionnaire_id' => "1"],
            ['id' => 3, 'question' => "Question 3", 'question_type' => "Text", 'questionnaire_id' => "1"],
            ['id' => 4, 'question' => "Question 4", 'question_type' => "Text", 'questionnaire_id' => "1"],
            ['id' => 5, 'question' => "Question 5", 'question_type' => "Text", 'questionnaire_id' => "1"],
        ]);
    }
}
