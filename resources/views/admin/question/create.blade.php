<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/app.css" />

    <title>@yield('title')</title>
  </head>
<body>
  <div class="container">

    <header class="row">
      <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
          <li class="name">
            <h1><a href="/">Questionnaire Builder</a></h1>
          </li>
        </ul>
        <section class="top-bar-section">
          <ul class="right">
            @if (Auth::guest())
            <li><a href="/login">Login</a></li>
            <li><a href="/register">Register</a></li>
            @else
            <li><a href="{{ url('/logout') }}">Logout</a></li>
            @endif
          </ul>
          <ul class="left">
            <li><a href="/questionnaire">Questionnaires</a></li>
            <li class="active"><a href="/questionnaire/create">Create</a></li>
          </ul>
        </section>
      </nav>
    </header>

    <article>

      <section class="row">
        <div class="large-8 columns">
          {!! Form::open(['url' => 'admin/question']) !!}

          <div class="form-group">
            {!! Form::label('questionnaire', 'Questionniare:') !!}
            {!! Form::select('questionnaire[]', $survey, null, ['class' => 'large-12 columns', 'multiple']) !!}
          </div>
          @for ($i=1;$i <= $questions[1]; $i++)
          <div class="form-group">
            {!! Form::label('question', 'Question ' . $i . ':') !!}
            {!! Form::text('question', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('question_type', 'Question Type:') !!}
            {!! Form::select('question_type', array('1' =>'Text', '2' => 'Multichoice', '3' => 'Checkboxes'), ['class' => 'form-control']) !!}
          </div>
          @endfor


            @if ($errors->any())
              <div data-alert class="alert-box alert">
                <ul class="no-bullet">
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div>
            @endif

            <div class="form-group large-6 columns">
              {!! Form::submit('Create Questions', ['class' => 'button']) !!}
            </div>

          {!! Form::close() !!}
      </div>
    </section>
  </article>
</div>
</body>
</html>
