@extends('layouts.master')

@section('title', 'Questionnaire Builder')

@section('content')
    <section class="row">
      <div class="large-12 columns">
        <h1>Questionnaires</h1>
      </div>
    </section>
    <section class="row">
      <div class="large-6 columns">
          <h5>Welcome</h5>
          <p>Welcome to my Questionnaire Builder. Click the Create New button to get started on a new questionnaire. Below will list all the ones you have created.</p>
      </div>
      <div class="large-6 columns">
        <a href="questionnaire/create" class="button success">Create New</a>
    </div>
  </section>
  <section class="row">
    <div class="large-12 columns">
      <h2>Recent Questionnaires</h2>
    </div>
  </section>
  @if (isset ($questionnaires))
  <div class="row">
    @foreach ($questionnaires as $questionnaire)
    <div class="large-3 columns end">
      <div class="panel">
        <h5>{{ $questionnaire->title }}</h5>
        <p>{{ $questionnaire->description }}</p>
        <a href="questionnaire/{{ $questionnaire->id }}" class="button success expand">Preview</a>
        <a href="questionnaire/{{ $questionnaire->id }}/edit" class="button expand">Update</a>
        {!! Form::open(['method' => 'DELETE', 'route' => ['admin.questionnaire.destroy', $questionnaire->id]]) !!}
        {!! Form::submit('Delete', ['class' => 'button expand alert']) !!}
        {!! Form::close() !!}
      </div>
    </div>
    @endforeach
  </div>
  @else
  <section class="row">
    <div class="large-5 columns panel">
      <h3>Nothing Here Yet</h3>
      <p>Try creating some new questionnaires for users to complete.</p>
    </div>
  </section>
  @endif

@endsection
