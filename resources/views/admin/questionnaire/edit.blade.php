<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/app.css" />

    <title>@yield('title')</title>
  </head>
<body>
  <div class="container">

    <header class="row">
      <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
          <li class="name">
            <h1><a href="/">Questionnaire Builder</a></h1>
          </li>
        </ul>
        <section class="top-bar-section">
          <ul class="right">
            @if (Auth::guest())
            <li><a href="/login">Login</a></li>
            <li><a href="/register">Register</a></li>
            @else
            <li><a href="{{ url('/logout') }}">Logout</a></li>
            @endif
          </ul>
          <ul class="left">
            <li><a href="/admin/questionnaire">Questionnaires</a></li>
            <li><a href="/admin/questionnaire/create">Create</a></li>
            <li class="active"><a href="#">Update</a></li>
          </ul>
        </section>
      </nav>
    </header>

    <article>

      <section class="row">
        <div class="large-12 columns">
          <h1>Edit Questionnaire</h1>
          <h3>{{ $questionnaire->title }}</h3>
        </div>
      </section>

      <section class="row">
        <div class="large-8 columns">



        {!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => 'admin/questionnaire/', $questionnaire->id]) !!}

          <div class="form-group">
            {!! Form::label('title', 'Change Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
          </div>

          <div class="form-group">
            {!! Form::label('description', 'Change Description:') !!}
            {!! Form::text('description', null, ['class' => 'form-control']) !!}
          </div>

          @if ($errors->any())
            <div data-alert class="alert-box alert">
              <ul class="no-bullet">
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <div class="form-group">
            {!! Form::submit('Update Questionnaire', ['class' => 'button']) !!}
          </div>

        {!! Form::close() !!}
      </div>
    </section>
  </article>
</div>
</body>
</html>
