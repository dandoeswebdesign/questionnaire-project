@extends('layouts.master')

@section('title', 'Questionnaire Builder')

@section('content')
    <section class="row">
      <div class="large-12 columns">
        <h1>{{ $questionnaire->title }}</h1>
      </div>
    </section>
    <section class="row">
      <div class="large-12 columns">
        <p>{{ $questionnaire->description }}</p>
      </div>
      @for ($i=1;$i <= $questions[1]; $i++)
        <p>{{ $questions[$i] }}
      @endfor
    </section>

@endsection
