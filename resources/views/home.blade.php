@extends('layouts.master')

@section('title', 'Questionnaire Builder')

@section('content')
    <section class="row">
      <div class="large-12 columns">
        <h1>Questionnaires</h1>
      </div>
    </section>
    <section class="row">
      <div class="large-6 columns">
          <h5>Welcome</h5>
          <p>Welcome to my Questionnaire Builder. Login or Register to create your own questionnaires. Below will list all the questionnaires you can complete without registering.</p>
      </div>
  </section>
  <section class="row">
    <div class="large-12 columns">
      <h2>Available Questionnaires</h2>
    </div>
  </section>
  @if (isset ($questionnaires))
    <section class="row">
      @foreach ($questionnaires as $questionnaire)
        <div class="large-3 columns end">
          <div class="panel">
            <h5>{{ $questionnaire->title }}</h5>
            <p>{{ $questionnaire->description }}</p>
            <a href="#" class="button success expand">Answer</a>
          </div>
        </div>
      @endforeach
    </section>
  @else
    <p>nothing here yet.</p>
  @endif

@endsection
