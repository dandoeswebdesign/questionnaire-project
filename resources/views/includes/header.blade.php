<nav class="top-bar" data-topbar role="navigation">
  <ul class="title-area">
    <li class="name">
      <h1><a href="#">Questionnaire Builder</a></h1>
    </li>
  </ul>

  <section class="top-bar-section">
    <ul class="left">
      <li class="active"><a href="#">Questionnaires</a></li>
      <li><a href="#">Create</a></li>
    </ul>
  </section>
</nav>
