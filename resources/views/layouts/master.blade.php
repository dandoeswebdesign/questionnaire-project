<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/css/app.css" />


    <title>@yield('title')</title>

    </head>
<body>
<div class="container">

    <header class="row">
      <nav class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
          <li class="name">
            <h1><a href="/">Questionnaire Builder</a></h1>
          </li>
        </ul>
        <section class="top-bar-section">
          <ul class="right">
            @if (Auth::guest())
            <li><a href="/login">Login</a></li>
            <li><a href="/register">Register</a></li>
            @else
            <li><a href="#">{{ Auth::user()->name }}</a></li>
            <li><a href="{{ url('/logout') }}">Logout</a></li>
            @endif
          </ul>
          <ul class="left">
            @if (Auth::guest())
            <li class="active"><a href="/">Questionnaires</a></li>

            @else
            <li class="active"><a href="questionnaire">Questionnaires</a></li>
            <li><a href="questionnaire/create">Create</a></li>
            @endif
          </ul>
        </section>
      </nav>
    </header>

    <article>

         @yield('content')

    </article>

</div>
</body>
</html>
